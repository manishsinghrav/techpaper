# **Elasticsearch**

Elasticsearch is a distributed, free, and open search and analytics engine for all types of data, including textual, numerical, geospatial, structured and, unstructured.

Elasticsearch is built on Apache Lucene and was first released in "2010" by Elasticsearch N.V.

## Uses of "Elasticsearch" 

**It can be used for numbers of cases**

* Application search
* Website search
* Logging and log analytics
* Application performance monitoring
* Security analytics
* Business analytics

## Working of "Elasticsearch" 

Raw data flows into **Elasticsearch** from a variety of sources, including logs, system metrics, and web applications. data ingestion is the process by which this raw data is parsed, normalized, and enriched before it is indexed in 'Elasticsearch'.
Once indexed in elastic search, we can run complex queries for their document and use aggregations to retrieve complex summaries of their documents.

## Elasticsearch index

An elastic search index is a collection of data that are related to each other. it stores data in JSON format. each data constructed as a set of keys with their associated values.
Elasticsearch uses an inverted index data structure, which is used very fast full-text search.
An inverted index lists every unique word that appears in any documents and identifies all of the documents each word occurs in.

## Why we should use "Elasticsearch"?

1. It is fast.
2. Elasticsearch is distributed by nature.
3. It comes with a wide set of features.
4. The elastic search simplifies data ingest visualization and reporting.

# Apache Solr

* It is an open-source search server platform, which is written in java language by apache software foundation.

* It is highly scalable and ready to deploy a search engine to handle large amounts of data.

## Purpose of using "apache solr"

The purpose of solr is to index and search a large amount of web content and give relevant content based on the search query.
It is a REST API-based HTTP wrapper around the full-text search engine called Apache Lucene.

## Lucene

It is a free and open-source search engine software library originally written in java language.

As mentioned, that it is a full-text search library in java which makes it easy to add search functionality to an application or website.

So by adding content to a full-text index,  then allows you to perform queries on this index. returning results ranked by either the relevance to the query or sorted by an arbitrary field such as the last modified document.
